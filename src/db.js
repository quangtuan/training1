"use strict";
exports.__esModule = true;
exports["default"] = {
    persons: [
        { name: 'Quang Tuan', age: 22, gender: 'Nam' },
        { name: 'Vuong Huan', age: 22, gender: 'Nam' },
        { name: 'Van Trinh', age: 22, gender: 'Nam' },
        { name: 'Duong Khuong', age: 22, gender: 'Nam' },
        { name: 'Ba Tri ', age: 24, gender: 'Nam' },
        { name: 'Thuy Ngoc', age: 22, gender: 'Nu' },
    ]
};
