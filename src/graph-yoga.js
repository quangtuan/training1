"use strict";
exports.__esModule = true;
var graphql_yoga_1 = require("graphql-yoga");
var db = require("./db");
var GraphYoga = /** @class */ (function () {
    function GraphYoga() {
        this.typeDefs = [
            "\n        type Query {\n            \n            person_name(name: String): [Person]\n            person_gender(gender: String): [Person]\n            person: [Person]\n        }\n        \n        type Person{\n            name: String!,\n            age: Int!,\n            gender: String!\n        }\n        "
        ];
        this.resolvers = {
            Query: {
                person_name: function (_, _a) {
                    var name = _a.name;
                    return db["default"].persons.filter(function (e) { return e.name == name; });
                },
                person_gender: function (_, _a) {
                    var gender = _a.gender;
                    return db["default"].persons.filter(function (e) { return e.gender == gender; });
                },
                person: function () {
                    return db["default"].persons;
                }
            }
        };
        this.server = new graphql_yoga_1.GraphQLServer({
            typeDefs: this.typeDefs,
            resolvers: this.resolvers
        });
    }
    return GraphYoga;
}());
exports["default"] = new GraphYoga().server;
